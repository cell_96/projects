package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import Commands.Get;
import Helper.Commands;
import driver.FileSystem;

public class TestGet {
  FileSystem FS;
  Commands get;

  @Before
  public void setUp() throws Exception {
    FS = new FileSystem("");
    get = new Get("");
  }

  @Test
  /*
   * test if get works with a valid link
   */
  public void testWithValidLink() throws Exception {
    String in = "get "
        + "https://mcs.utm.utoronto.ca/~zingarod/236/lecture6/dan.txt";
    get.setInput(in);
    String execute = get.execute(FS);
    boolean value = FS.getChildrenFile().containsKey("dan.txt");
    assertEquals(true, value);

  }
  
  @Rule
  public ExpectedException exception = ExpectedException.none();
  
  @Test
  /*
   * test if Get catches exceptions properly
   */
  public void testNoProtocol() throws Exception {
    String in = "get "
        + "/https://mcs.utm.utoronto.ca/~zingarod/236/lecture6/dan.txt";
    get.setInput(in);
    exception.expect(Exception.class);
    exception.expectMessage("no protocol: /https://mcs.utm.utoronto.ca/"
        + "~zingarod/236/lecture6/dan.txt does not exist");
    String execute = get.execute(FS);
    String value = "no protocol: /https://mcs.utm.utoronto.ca/"
        + "~zingarod/236/lecture6/dan.txt does not exist";
    assertEquals(value, execute);

  }

}
