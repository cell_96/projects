-- ANEES BAJWA: 1001849556
-- JOSEPH CHARLES: 1001535797

--A)
	SELECT model
	FROM PC
	WHERE speed >= '3.00';

--B)
	SELECT maker
	FROM PRODUCT NATURAL JOIN LAPTOP
	WHERE hd >= '100';

--C)
	SELECT model, price
	FROM ((SELECT model, price FROM PC)
		 UNION (SELECT model, price FROM PRINTER)
		 UNION (SELECT model, price FROM LAPTOP))
		 NATURAL JOIN PRODUCT
	WHERE maker = 'B';

--D)
	SELECT model
	FROM PRINTER
	WHERE color = 'TRUE' and type = 'laser';

--E)
	SELECT distinct maker
	FROM PRODUCT
	WHERE type = 'laptop'
	MINUS
	SELECT distinct maker
	FROM PRODUCT
	WHERE type = 'pc';

--F)
	SELECT pc1.model as PC1, pc2.model as PC2
	FROMQ pc  pc1 , pc  pc2
	WHERE pc1.ram = pc2.ram and pc1.speed = pc2.speed
		  and pc2.model > pc1.model;

--G)
	SELECT avg(price) as AVG_PRICE
	FROM ((SELECT model, price FROM PC)
		 UNION (SELECT model, price FROM LAPTOP))
		 NATURAL JOIN PRODUCT
	GROUP BY maker
	HAVING maker = 'D';

--H)
	SELECT maker
	FROM PC NATURAL JOIN PRODUCT
	GROUP BY maker
	HAVING COUNT(model) >= '3';

--I)
	SELECT distinct maker
	FROM product
	WHERE model between 3001 and 3007 and type = 'printer';

--J)
	UPDATE laptop SET price = (price - 100);
	UPDATE laptop SET screen = (screen + 1);

--K)
	DELETE FROM PRODUCT
	WHERE model in (SELECT PRODUCT.model FROM PRODUCT,PRINTER
					WHERE PRINTER.color = 'FALSE'
					AND PRINTER.model = PRODUCT.model);
