// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import Commands.Get;
import Helper.Commands;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class TestGet {
  FileSystem FS;
  Commands get;

  @Before
  public void setUp() throws Exception {
    FS = new FileSystem("");
    get = new Get("");
  }

  @Test
  /*
   * test if get works with a valid link
   */
  public void testWithValidLink() throws Exception {
    String in = "get "
        + "https://mcs.utm.utoronto.ca/~zingarod/236/lecture6/dan.txt";
    get.setInput(in);
    String execute = get.execute(FS);
    boolean value = FS.getChildrenFile().containsKey("dan.txt");
    assertEquals(true, value);

  }
  
  @Rule
  public ExpectedException exception = ExpectedException.none();
  
  @Test
  /*
   * test if Get catches exceptions properly
   */
  public void testNoProtocol() throws Exception {
    String in = "get "
        + "/https://mcs.utm.utoronto.ca/~zingarod/236/lecture6/dan.txt";
    get.setInput(in);
    exception.expect(Exception.class);
    exception.expectMessage("no protocol: /https://mcs.utm.utoronto.ca/"
        + "~zingarod/236/lecture6/dan.txt does not exist");
    String execute = get.execute(FS);
    String value = "no protocol: /https://mcs.utm.utoronto.ca/"
        + "~zingarod/236/lecture6/dan.txt does not exist";
    assertEquals(value, execute);

  }

}
