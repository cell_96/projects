-- ANEES BAJWA: 1001849556
-- JOSEPH CHARLES: 1001535797

--INITIALIZE THE PACKAGE
CREATE OR REPLACE PACKAGE SP_SPECS IS

PROCEDURE INSERT_SP( S_NO SP.SNO%TYPE , P_NO SP.PNO%TYPE, QUANTITY SP.QTY%TYPE);
PROCEDURE GET_SNAME_2PARTS( S_NO SP.SNO%TYPE , RESULT OUT VARCHAR2);
FUNCTION FIND_MAX_QTY_SUPPLIED( S_NO SP.SNO%TYPE) RETURN NUMBER ;
FUNCTION FIND_SNAME_GIVEN_SNO( S_NO SP.SNO%TYPE) RETURN VARCHAR2 ;
PROCEDURE REPORT_SUPPLIERS;

END SP_SPECS;
/

--ADD THE BODY
CREATE OR REPLACE PACKAGE BODY SP_SPECS as

PROCEDURE INSERT_SP( S_NO SP.SNO%TYPE , P_NO SP.PNO%TYPE, QUANTITY SP.QTY%TYPE) is
BEGIN
  INSERT INTO SP VALUES(S_NO, P_NO, QUANTITY);
END INSERT_SP;


PROCEDURE GET_SNAME_2PARTS( S_NO SP.SNO%TYPE , RESULT OUT VARCHAR2) is
CURSOR supply is SELECT sno, sname, COUNT(PNO) from SP natural join s GROUP BY sno, sname;
total NUMBER;
snoo VARCHAR2(3 BYTE);
snamee VARCHAR2(10 BYTE);
BEGIN
  open supply;
  LOOP
    FETCH supply into snoo, snamee, total;
    EXIT WHEN supply%notfound;

    If S_NO = snoo then
      if total = 2 then
        result := 'TRUE';
        dbms_output.put_line(snamee || ' supplies exactly 2 parts');
      else
        result := 'FALSE';
        dbms_output.put_line('This supplier does not supply 2 parts');
      end if;
    end if;
  end loop;
  close supply;

END GET_SNAME_2PARTS;


FUNCTION FIND_MAX_QTY_SUPPLIED( S_NO SP.SNO%TYPE) RETURN NUMBER is
  max_qty NUMBER;
  sno2 VARCHAR2(3 BYTE);
  cursor getmax is SELECT sno, max(qty) FROM SP GROUP BY sno;
BEGIN
  open getmax;
  LOOP
    FETCH getmax into sno2, max_qty;
    EXIT WHEN getmax%notfound;

    if S_NO = sno2 then
      RETURN max_qty;
    end if;
  END LOOP;
  close getmax;
END;


FUNCTION FIND_SNAME_GIVEN_SNO( S_NO SP.SNO%TYPE) RETURN VARCHAR2 is
  snamee VARCHAR2(10 BYTE);
  snoo VARCHAR2(3 BYTE);
  cursor getname is SELECT sno, sname FROM SP NATURAL JOIN S;
BEGIN
  open getname;
  LOOP
    FETCH getname into snoo, snamee;
    EXIT WHEN getname%notfound;

    if S_NO = snoo then
      RETURN snamee;
    end if;

  END LOOP;
  close getname;
END FIND_SNAME_GIVEN_SNO;


PROCEDURE REPORT_SUPPLIERS is
snamee VARCHAR2(10 byte);
qtyy NUMBER;
cursor totalqty is SELECT sname,SUM(qty) FROM SP Natural Join S GROUP by sname;
BEGIN
  open totalqty;
  dbms_output.put_line('All suppliers');
  dbms_output.put_line('--------------------------------------');
  LOOP
    FETCH totalqty into snamee, qtyy;
    EXIT WHEN totalqty%notfound;

    dbms_output.put_line('Supplier ' || snamee || ' supplies ' || qtyy);

  END LOOP;
  close totalqty;
END REPORT_SUPPLIERS;

END SP_SPECS;
/
