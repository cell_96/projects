// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package Commands;

import Helper.*;
import driver.*;

public class History extends Commands {
  public History(String userInput) {
    super.setInput(userInput);
  }
  /**
   * Returns the history of the commands the user inputed
   * 
   * @param log is an log object
   * @throws Exception
   * @return empty string, cause of polymorphism
   */
  public String execute(Log log) throws Exception {
    if (getArguments().length == 0) { //If the length is zero
      return log.printHistory(log.getSize()).trim();
    } else {
      try { //try block
        return log.printHistory(Integer.parseInt(getArgumentAtPosition(0)))
            .trim();
      } catch (NumberFormatException e) {
        throw new Exception(getArgumentAtPosition(0) + " is not an integer");
      } catch (Exception e) {
        throw new Exception(e.getMessage());
      }
    }
  }
}
