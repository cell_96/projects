-- ANEES BAJWA: 1001849556
-- JOSEPH CHARLES: 1001535797

--5A
CREATE OR REPLACE TRIGGER ensure_case
BEFORE INSERT OR UPDATE ON S
FOR EACH ROW
BEGIN
  :NEW.SNAME := UPPER(:new.SNAME);
  :NEW.CITY := UPPER(:new.CITY);
END;
/

--5B
CREATE OR REPLACE TRIGGER change_msg
AFTER UPDATE ON S
FOR EACH ROW
BEGIN
  dbms_output.put_line('Supplier name ' || :old.sname || ' has changed to ' || :new.sname);
END;
/
