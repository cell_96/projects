#**********************************************************
# Assignment2:test_anagram.py
# UTOR user_name:bajwaane
# First Name:Anees
# Last Name:Bajwa
# Student #1001849556
#
#
#
# Honour Code: I pledge that this program represents my own
# Page 3 of 4CSC148H Winter 2015 Assignment #2
# program code and that I have coded on my own. I received
# help from no one in designing and debugging my program.
# I have also read the plagiarism section in the course info sheet
# of CSC 148 and understand the consequences.
#**********************************************************
import unittest
from anagram import *

class test_anagram_generator(unittest.TestCase):

    def test_max_zero(self):
        """I chose this case becasue if max is 0, it should return everything
        which is an important case for the assignment"""
        
        words = list_generator('dict.txt')
        anagram = AnagramSolver(words)
        results = anagram.generateAnagrams('office',0)
        self.assertEqual(results,[['ice', 'off'], ['off', 'ice'], ['office']])
        

    def test_max_two(self):
        """I chose this case because if max is 2, it should return every list of elements
        that have a length of max or less, which is quite important"""
        
        words = list_generator('dict.txt')
        anagram = AnagramSolver(words)
        results = anagram.generateAnagrams('office key',2)
        self.assertEqual(results,[['key', 'office'], ['office', 'key']])


    def test_punctuation_string(self):
        """I chose this case because if the string has punc, it should ignore the punc,
        this is important becuase the punctuation should not matter to your answer"""
        
        words = list_generator('dict.txt')
        anagram = AnagramSolver(words)
        results = anagram.generateAnagrams('office!!!',2)
        self.assertEqual(results,[['ice', 'off'], ['off', 'ice'], ['office']])
        
        
    def test_number_string(self):
        """I chose this case because if the string has numbers, means no alpha char
        which will give u empty list, important cause it only looks at alpha char"""
        
        words = list_generator('dict.txt')
        anagram = AnagramSolver(words)
        results = anagram.generateAnagrams('555',0)
        self.assertEqual(results,[])


    def test_max_less_zero(self):
        """I chose this case because if max is less than 0, it should return a error
        becuase len of a list cant be less than 0, this is an important case for the assig"""
        
        words = list_generator('dict.txt')
        anagram = AnagramSolver(words)
        checker = self.assertRaises(ValueError,anagram.generateAnagrams,"office",-7)
        self.assertRaises(ValueError,checker)
    

if __name__ == '__main__':
    unittest.main(exit=False)

        
