(* QUESTION ONE *)

exception notSameLength;

(* Pattern Matching *)
(*If both list empty, than u want to return empty, if not same length, raise an error, else do the computation and add to list*)
fun listSum2(A:real, [], []):(real list) = []
| listSum2(A:real, [], a::b) = raise notSameLength (*If not same length*)
| listSum2(A:real, a::b, []) = raise notSameLength
| listSum2(A:real, a::[], c::[]) = [A + (a*c)]
| listSum2(A:real, a::b, c::d) = [A + (a*c)]@listSum2(A, b, d); (*compute and tranverse*)

(* Without *)
fun listSum1(A:real, X:(real list), Y:(real list)):(real list) =
if length(X) <> length(Y) then raise notSameLength (*If length not same*)
else if null(X) then [] else [A + (hd(X)*hd(Y))]@listSum1(A,tl(X),tl(Y));

(* QUESTION TWO *)

(* a) *)
type student = {id:int, name:string, gpa:real};

(* b) *)
type taken = {course:string, student:int, grade:real};

(* c) *)
exception lessThanZero; (*If new gpa is less than 0*)
fun updateGpa(G:real, {id = a, name = b, gpa = c}:student):student =
if G < 0.0 then raise lessThanZero
else {id = a, name = b, gpa = G};

(* d) *)
exception notFound;
fun getInfo({course = c, student = s, grade = _}:taken) = (c,s); (*Tuple of the course and student id*)

fun updateGrade(T as {course = c, student = s, grade = g}:taken, Tlist:(taken list)):(taken list) =
if null(Tlist) then raise notFound else
    if getInfo(hd(Tlist)) = (c,s) then T::tl(Tlist) (*if user found in tlist, than change new grade*)
    else hd(Tlist)::updateGrade(T, tl(Tlist));

(* e) *)
fun checkId({id = a, name=b,gpa=_}:student, {course=c,grade = _,student=d}:taken) = (*function is to check if student id from taken and student are same*)
if a = d then [b] else [];
fun getcName{course=c,grade = _,student=_} = c (*Get course name*)

exception StudentNotInSlist; (*If course taken but id not in student list*)
fun check(C:string, Slist:(student list), Tvalue:taken) =
    if null(Slist) then raise StudentNotInSlist else
        if checkId(hd(Slist), Tvalue) = [] then (*If id not same go to next record, if it does than return list of name*)
            check(C, tl(Slist), Tvalue) else
        checkId(hd(Slist), Tvalue);


fun names(C:string, Slist:(student list), Tlist:(taken list)) =
if null(Tlist) then [] else (*if it has same course, than check each record of takenlist and see if its in studentlist*)
    if getcName(hd(Tlist)) = C then check(C, Slist, hd(Tlist))@names(C, Slist, tl(Tlist)) else
        names(C, Slist, tl(Tlist));

(* f) *)
exception noClasses; (*If student is taking no classes*)
fun getMark({course=_,student=_,grade=b}:taken) = b; (*Gets mark of student from taken datatype*)
fun getId({course = _, student = a, grade = _}:taken) = a; (*Gets the id of student from taken datatype*)

fun count(i:int,tlist:(taken list)) = (*this function counts the number of class the student is taking*)
if null(tlist) then 0.0 else
    if getId(hd(tlist)) = i then 1.0 + count(i,tl(tlist)) else count(i,tl(tlist));

fun computeGPA(I:int, Tlist:(taken list)) =
let fun helper(I:int, Tlist:(taken list), number:real) = (*This function computes the new average(gpa) with the course mark, if student taking no classes, raise exception*)
    if number > 0.0 then
        if null(Tlist) then 0.0 else
            if getId(hd(Tlist)) = I then getMark(hd(Tlist))/number + helper(I,tl(Tlist), number) else
                helper(I,tl(Tlist), number)
    else raise noClasses;
in
    helper(I,Tlist,count(I, Tlist))
end;

(* QUESTION THREE *)

fun getGpa({id = _, name = _, gpa = c}:student) = (c); (*Get gpa from student*)
fun getSid({id = a, name = _, gpa = _}:student) = a; (*Get id from student*)
fun getSName({id = a, name = b, gpa = _}:student) = b; (*Get name from student*)

(*if an erro is caught, than store the student record in a diffrent list
if no error, change gpa and store in diff list, at end put both the list in a tuple*)
fun updateAllGPAs(slist, tlist) =
let fun exceptionCheck(slist, tlist, updatedList:(student list), exceptionList:(student list)) =
    if null(slist) then (updatedList, exceptionList) else
        exceptionCheck(tl(slist), tlist, {id = getSid(hd(slist)), name = getSName(hd(slist)), gpa = computeGPA(getSid(hd(slist)),tlist)}::updatedList, exceptionList)
        handle noClasses => (exceptionCheck(tl(slist), tlist, updatedList, {id = getSid(hd(slist)), name = getSName(hd(slist)), gpa = getGpa(hd(slist))}::exceptionList));
in
    exceptionCheck(slist, tlist, [], [])
end;


(* QUESTION FOUR  *)

exception infinityException; (*If trying to compute various infinity cases that will not give results*)
exception divisionError; (*If trying to divide 0 by 0*)

(* a) *)
datatype eInt = numb of int | inf of int;

(* b) *)
fun eAdd(numb n, numb k) = numb(n+k) (*both numbers, just compute*)
| eAdd(inf n, inf k) = if (n < 0 andalso k > 0) orelse (*If trying to infinity + (- infinity) raise exception*)
    (n > 0 andalso k < 0) then raise infinityException else if (n > 0 andalso k > 0) then (*inf + inf and -inf + (- inf) will work*)
    inf(1) else inf(~1)
| eAdd(numb n, inf k) = if k < 0 then inf(~1) else inf(1)
| eAdd(inf k, numb n) = if k < 0 then inf(~1) else inf(1);

(* c) *)
fun eSub(numb n, numb k) = numb(n - k)
| eSub(inf n, inf k) = if (n > 0 andalso k > 0) orelse (*if trying to infinity - infinity raise exception*)
    (n > 0 andalso k < 0) then raise infinityException else if (n < 0 andalso k > 0) then
    inf(~1) else inf(1)
| eSub(numb n, inf k) = if k < 0 then inf(1) else inf(~1) (*if negative infinity then return positive, else negative*)
| eSub(inf k, numb n) = if k < 0 then inf(1) else inf(~1);

(* d) *)
fun eMult(numb n, numb k) = numb(n * k)
| eMult(inf n, inf k) = if (n < 0 andalso k > 0) orelse (n > 0 andalso k < 0) then inf(~1) else
    if n < 0 andalso k < 0 then inf(1) else inf(1) (*-inf * -inf will give positive inf*)
| eMult(numb n, inf k) = if n = 0 then raise infinityException else (*infinity * 0 will give error*)
    if (n < 0 andalso k > 0) orelse (n > 0 andalso k < 0) then inf(~1) else inf(1)
| eMult(inf k, numb n) = if n = 0 then raise infinityException else
    if (n < 0 andalso k > 0) orelse (n > 0 andalso k < 0) then inf(~1) else inf(1);

(* e) *)
fun eDiv(numb n, numb k) = if n = 0 andalso k = 0 then raise divisionError else if k = 0 then if n < 0 then inf(~1) else inf(1) else numb(n div k) (*0/0 will give exception*)
| eDiv(inf n, inf k) = raise infinityException (*inf/inf will also result an exception*)
| eDiv(numb n, inf k) = numb(0)
| eDiv(inf k, numb n) = if n < 0 then inf(~1) else inf(1);

(* f) *)
(* i. *)        eSub(numb(13),eDiv(numb(6),numb(0)));
(* ii. *)       eMult(numb(3), eDiv(numb(1),numb(0)));
(* iii. *)      eMult(eDiv(numb(~2),numb(0)), eDiv(numb(~6),numb(0)));
(* iv. *)       eAdd(eDiv(numb(5),numb(0)), eDiv(numb(~3),numb(0))); *)
(* v. *)        eDiv(eDiv(numb(7),numb(0)), eDiv(numb(3), eSub(numb(2),numb(2)))); *)
(* vi. *)       eDiv(numb(3), eDiv(numb(~4), eDiv(numb(5), numb(0))));
(* vii. *)      eDiv(numb(3), eDiv(numb(~4), eDiv(numb(5), eDiv(numb(~6),numb(0)))));
(* viii. *)     eAdd(numb(4), eMult(numb(7), eDiv(numb(9), eSub(numb(8),numb(5)))));
(* ix. *)       eMult(eSub(numb(4), eAdd(numb(3),numb(1))), eDiv(numb(3), eAdd(numb(4), numb(~4)))); *)
(* x. *)        eDiv(eSub(numb(4),numb(4)), eSub(numb(6), eAdd(numb(3), numb(3)))); *)        


(* QUESTION FIVE *)

(* a) *)
datatype 'a tree =  inNode1 of 'a * 'a tree | inNode2 of 'a * 'a tree * 'a tree | inNode3 of 'a * 'a tree * 'a tree  * 'a tree | leafReal of real | leafList of string list;

(* b) *)
(*On diffrent document: Question5b.pdf*)

(* c) *)
fun list12(leafReal n) = [] (*If leaf than nothing*)
| list12(leafList n) = []
| list12(inNode2(a, left, right)) = a::list12(left)@list12(right) (*If 2 than u just want to add the node and loop through the child*)
| list12(inNode1(a, left)) = a::list12(left)
| list12(inNode3(a, left, middle, right)) = list12(left)@list12(middle)@list12(right); (*If 3 than u just want to go through it*)


(* d) *)
fun addtup((x,y,z), (x2,y2,z2), (x3,y3,z3)) =  (*This function takes in 3 tuples an adds all of them by its elements*)
(x+x2+x3, y+y2+y3, z+z2+z3);

(*This function returns the tuple of counts, if its leaf then dont change any values,
if it has more than 1 children, pass two tuple which will add the values*)
(*This function returns the tuple of counts, if its leaf then dont change any values,
if it has more than 1 children, pass two tuple which will add the values*)
fun countNodes(T) =
let fun countNHelp(leafReal(n), n1:int, n2:int, n3:int) = (n1, n2, n3)
| countNHelp(leafList(n), n1, n2, n3) = (n1, n2, n3)
| countNHelp(inNode1(a, left), n1, n2, n3) = countNHelp(left, n1+1, n2, n3) (*there is no point of calling function, as only 1 tuple*)
| countNHelp(inNode2(a, left, right), n1, n2, n3) = addtup(countNHelp(left, n1, n2, n3), countNHelp(right, 0, 1, 0), (0,0,0)) (*(0,0,0) because we dont have another tree*)
| countNHelp(inNode3(a, left, middle, right), n1, n2, n3) = addtup(countNHelp(left, n1, n2, n3), countNHelp(right, 0, 0, 1),countNHelp(middle, 0, 0, 0)) ;
in
countNHelp(T, 0,0,0)
end;

(* e) *)
fun treeApply(F, leafReal n) = F(n) (*If leaf is type real than call function and do computation*)
| treeApply(F, leafList n) = 0.0
| treeApply(F, inNode2(g, left, right)) = treeApply(F, left) + treeApply(F, right) (*If not leaf, just tranverse*)
| treeApply(F, inNode1(g, left)) = treeApply(F, left)
| treeApply(F, inNode3(g, left, middle, right)) = treeApply(F, left)+ treeApply(F, middle)+ treeApply(F, right);

(* f) *)
fun leafAppend(L:(string list), leafReal n) = leafReal(n)
| leafAppend(L:(string list), leafList n) = leafList(L@n) (*If leaf is type list, than append L to its list*)
| leafAppend(L:(string list), inNode2(a, left, right)) = inNode2(a, leafAppend(L, left), leafAppend(L, right)) (*If not just add content to new tree normally*)
| leafAppend(L:(string list), inNode1(a, left)) = inNode1(a, leafAppend(L, left))
| leafAppend(L:(string list), inNode3(a, left, middle, right)) = inNode3(a, leafAppend(L, left), leafAppend(L, middle), leafAppend(L, right));

(* QUESTION SIX *)
exception noSuchLength; (*If N is higher than list length*)

(* a) *)
datatype list1 = nil1 | cons1 of int*list1;

fun remList1(N, nil1) = if N <> 0 then raise noSuchLength else nil1
| remList1(N, cons1(a, k)) = if N = 1 then k else cons1(a, remList1(N-1, k)); (* if one than ignore a, pass k*)

(* b) *)
datatype list2 = nil2 | cons2 of int*(list2 ref);
fun cdr2(nil2) = raise noSuchLength
| cdr2(cons2(a,k)) = k;

fun remList2(N, RL:list2 ref) = if N = 1 then RL := !(cdr2(!RL)) (*move to the next spot if N is 1*)
else remList2(N-1, cdr2(!RL)); (*if not 1, make n 1 less and pass next refrence*)

(* c) *)
fun remList3(N, RL:list2 ref) =
let
    val refList = ref RL (*double refrence RL*)
    val x = ref N
in
    while (!x) > 1 do (*when N is 1, want to skip that cons and move to one after it*)
        (x := (!x) - 1; (*subtract 1 from x*)
        refList := cdr2(!(!refList))); (*keep moving across the list*)
    !refList := !(cdr2(!(!refList)))
end;
