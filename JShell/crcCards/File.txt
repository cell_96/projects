Class name: File
Parent class: None
Subclass: None

Responsibilities:
-create a new file
-add content to an existing file
-get contents of an existing file
-get name of an existing file

Collaborations:
-None