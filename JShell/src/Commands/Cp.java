// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package Commands;

import java.util.ArrayList;
import java.util.Arrays;
import Helper.Commands;
import driver.FileSystem;

/**
 * 
 * @author Raj
 *
 */
public class Cp extends Commands {
  private ArrayList<String> specialCharacter =
      new ArrayList<String>(Arrays.asList(".", "..", "/"));

  public Cp(String userInput) {
    super.setInput(userInput);
  }

  /**
   * Return an empty string, for polymorphism
   *
   * @param fs filesystem object
   * @throws Exception
   */
  public String execute(FileSystem fs) throws Exception {
    String[] oldPath = convertArrayString(getArgumentAtPosition(0));
    String dirNameOldPath = getDirName(getArgumentAtPosition(0));
    String[] newPath = convertArrayString(getArgumentAtPosition(1));
    String dirNameNewPath = getDirName(getArgumentAtPosition(1));
    FileSystem newFS = getPathHandler().toThePath(fs, oldPath,
        getArgumentAtPosition(0).startsWith("/"));
    FileSystem oldFS = getPathHandler().toThePath(fs, newPath,
        getArgumentAtPosition(1).startsWith("/"));
    if ((oldFS.isFSExist(dirNameOldPath) && newFS.isFSExist(dirNameNewPath))
        || specialCharacter.contains(dirNameOldPath)
        || specialCharacter.contains(dirNameNewPath)) {
      newFS = getPathHandler().toThePath(newFS, new String[] {dirNameNewPath},
          false);
      moveDirToDir(oldFS, newFS, dirNameOldPath);
    } else if ((oldFS.isFileExist(dirNameOldPath)
        && newFS.isFSExist(dirNameNewPath))
        || specialCharacter.contains(dirNameNewPath)) {
      moveFileToDir(oldFS, newFS, dirNameOldPath, dirNameNewPath);
    } else if (oldFS.isFileExist(dirNameOldPath)
        && newFS.isFileExist(dirNameNewPath)) {
      moveFileToFile(oldFS, newFS, dirNameOldPath, dirNameNewPath);
    } else {
      throw new Exception("CP command cannot work"); // throw exception
    }

    return null;
  }

  /**
   * Move the file to the directory that the user inputs
   *
   * @param oldFS FileSystem object
   * @param newFS FileSystem object
   * @param dirNameOldName is the String name of the old dir
   * @param dirNameNewPath is the path of the new one
   * @throws Exception
   * 
   */
  private void moveFileToFile(FileSystem oldFS, FileSystem newFS,
      String dirNameOldPath, String dirNameNewPath) throws Exception {
    if (oldFS.isFileExist(dirNameOldPath)) {
      String newContent =
          oldFS.getChildrenFile().get(dirNameOldPath).getContent();
      newFS.addFile(dirNameOldPath, false, newContent);
    } else {
      throw new Exception("file" + dirNameOldPath + " does not exist!");
    }
  }

  /**
   * Return a array of the string
   *
   * @param covert the string to array
   * @return the array string
   */
  private String[] convertArrayString(String path) {
    String[] newPath = path.split("/");
    if (newPath.length > 0) {
      int initLen;
      int lastLen = newPath.length - 1;
      if (path.startsWith("/")) {
        initLen = 1;
      } else {
        initLen = 0;
      }
      return Arrays.copyOfRange(newPath, initLen, lastLen);
    } else {
      return new String[] {"."};
    }
  }

  /**
   * get the directory name
   *
   * @param path user path input
   * @return the name of the directory
   */
  private String getDirName(String path) {
    if (path.split("/").length > 0)
      return path.split("/")[path.split("/").length - 1];
    else
      return ".";
  }

  /**
   * Move the dir to dir without deleting
   *
   * @param path is to set path
   * @param oldpath FileSystem object
   * @param newPath FileSystem object
   * @param dirName is the String name of the directory
   * @throws Exception
   */
  private void moveDirToDir(FileSystem oldPath, FileSystem newPath,
      String dirName) throws Exception {
    if (oldPath.getAllChildrenName().isEmpty()) {
      return;
    } else {
      oldPath = getPathHandler().toThePath(oldPath, new String[] {dirName},
          dirName.equals("/"));
      dirName = new String(oldPath.getName());
      newPath.addFileSystem(dirName);
      newPath = getPathHandler().toThePath(newPath, new String[] {dirName},
          dirName.equals("/"));
      if (oldPath.getChildrenFile() != null) {
        for (String fileName : oldPath.getChildrenFile().keySet()) {
          newPath.addFile(fileName, true,
              oldPath.getChildrenFile().get(fileName).getContent());
        }
      }
      if (oldPath.getChildrenFS() != null) {
        for (String dir : oldPath.getChildrenFS().keySet()) {

          moveDirToDir(oldPath, newPath, dir);
        }
      }
    }
  }

  /**
   * Move the file to the directory that the user inputs
   *
   * @param oldpath FileSystem object
   * @param newPath FileSystem object
   * @param dirOldName is the String name of the old dir
   * @param dirNewPath is the path of the new one
   * @throws Exception
   * 
   */
  private void moveFileToDir(FileSystem oldPath, FileSystem newPath,
      String dirOldName, String dirNewPath) throws Exception {
    newPath = getPathHandler().toThePath(newPath, new String[] {dirNewPath},
        dirNewPath.equals("/"));
    // If it contains the old dir
    if (newPath.getAllChildrenName().contains(dirOldName)) {
      return; // throw exception
    } else {
      newPath.addFile(dirOldName, true,
          oldPath.getChildrenFile().get(dirOldName).getContent());
    }
  }
}

