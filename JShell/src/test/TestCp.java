// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import Commands.CD;
import Commands.Cp;
import Commands.LS;
import Commands.MkDir;
import Commands.PWD;
import Helper.Commands;
import Helper.FileCreator;
import Helper.PathHandler;
import driver.FileSystem;

/*
 * Author: Raj Shah
 */
public class TestCp {
  Commands mkdir;
  Commands ls;
  Cp copy;
  CD cd;
  Commands pwd;
  FileSystem system;
  FileCreator creator;
  Commands test;
  PathHandler path;
  FileSystem FS;
  String[] args;
  String executer;



  @Before
  public void setUp() throws Exception {
    mkdir = new MkDir("");
    path = new PathHandler();
    FS = new FileSystem("root");
    ls = new LS("");
    pwd = new PWD("");
    system = new FileSystem("");
    creator = new FileCreator("");
    test = new Commands();
    // copy = new Cp("");
  }

  @Test
  // Cp one dir to dir
  public void test_copy_dir() throws Exception {
    args = new String[3];
    args[0] = "Folder";
    args[1] = "Folder2";
    args[2] = "Folder3";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    copy = new Cp("cp Folder2 Folder");
    copy.execute(FS);
    cd = new CD("cd Folder");
    FS = cd.changPath(FS);
    executer = ls.execute(FS);
    assertEquals("Folder2", executer);
  }

  @Test
  // Copy to dirs to another dir
  public void test_copy_mult_dir() throws Exception {
    args = new String[4];
    args[0] = "Folder";
    args[1] = "Folder2";
    args[2] = "Folder3";
    args[3] = "Folder4";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    copy = new Cp("cp Folder2 Folder");
    Cp copy2 = new Cp("cp Folder4 Folder");
    copy.execute(FS);
    copy2.execute(FS);
    cd = new CD("cd Folder");
    FS = cd.changPath(FS);
    executer = ls.execute(FS);
    assertEquals("Folder2\tFolder4", executer);
  }

  @Test
  // Test of copying a file to a new dir
  public void test_copy_file() throws Exception {
    args = new String[2];
    args[0] = "Folder";
    args[1] = "Folder2";
    mkdir.setArgument(args);
    mkdir.execute(FS);

    Commands main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(FS);
    creator.execute(FS, test.getArguments(), executer);
    ls.setArgument(new String[0]);

    copy = new Cp("cp file Folder");
    copy.execute(FS);
    // This shows that it does not get deleted of main
    String check = ls.execute(FS);
    assertEquals("Folder2\tFolder\tfile", check);

    cd = new CD("cd Folder");
    FS = cd.changPath(FS);
    String cuter = ls.execute(FS);
    assertEquals("file", cuter);
  }

  @Test
  // Test copying dir that does not exist
  public void test_error_Dir() throws Exception {
    args = new String[4];
    args[0] = "Folder";
    args[1] = "Folder2";
    args[2] = "Folder3";
    args[3] = "Folder4";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    copy = new Cp("cp Folder4 Folder");
    copy.execute(FS);
    ExpectedException exception = ExpectedException.none();
    exception.expectMessage("CP command cannot work");
  }
  
  @Test
  // Test copying file that does not exist
  public void test_error_File() throws Exception {
    args = new String[2];
    args[0] = "Folder";
    args[1] = "Folder2";
    mkdir.setArgument(args);
    mkdir.execute(FS);

    Commands main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(FS);
    creator.execute(FS, test.getArguments(), executer);
    ls.setArgument(new String[0]);

    copy = new Cp("cp file Folder");
    copy.execute(FS);
    ExpectedException exception = ExpectedException.none();
    exception.expectMessage("CP command cannot work");
  }
}
