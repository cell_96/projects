// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
/*
 * author: Anees Bajwa
 */
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Helper.File;

public class TestFile {
  File file;
  String content;

  @Before
  public void setUp() throws Exception {
    file = new File("filename");
  }

  private String addContent(boolean isOverwrite, String input){
    if (isOverwrite) {
      content = input;
    } else {
      content += "\n" + input;
    }
    return content;
  }
   
  @Test
  public void testAddContent() {
    String fileContent1 = "hello my name is";
    String fileContent2 = " bob";
    String combined = fileContent1 + "\n" + fileContent2;
    
    assertEquals(fileContent1, addContent(true, fileContent1));
    assertEquals(combined, addContent(false, fileContent2));
  }
  
}
