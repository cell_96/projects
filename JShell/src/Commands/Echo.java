// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import Helper.*;
import driver.*;

/**
 * @author Anees Bajwa
 *
 */
public class Echo extends Commands {

  /**
   * class constructor
   */
  public Echo(String userInput) {
    super.setInput(userInput); // set commands by the super class method
  }

  /**
   * set the string of file content to be added to file
   * 
   * @param newInput a string from user
   */
  public void setInput(String userInput) {
    setCommand("echo"); // Set command
    String newInput = userInput.split("echo")[1].trim();
    int initIndex;
    int lastIndex = 0;
    if (newInput.startsWith("\"") && newInput.endsWith("\"")){

      // check quotation marks for file's content
      initIndex = newInput.indexOf("\"") + 1;
      lastIndex = newInput.lastIndexOf("\"");
    } else { // if we do not have quotation marks surrounding around content
      initIndex = 0;
      lastIndex = newInput.length();
    }
    if (lastIndex == initIndex) {
      newInput = "";
    } else {
      newInput = newInput.substring(initIndex, lastIndex);
    }
    setArgument(new String[] {newInput});
  }


  /**
   * print out the string from user
   *
   * @param fs a file system object
   * 
   * @return return the string 
   */
  public String execute(FileSystem fs) {
    return getArgumentAtPosition(0);
  }
}
