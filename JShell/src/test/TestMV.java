// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.CD;
import Commands.Cp;
import Commands.MV;
import Commands.LS;
import Commands.MkDir;
import Commands.PWD;
import Helper.Commands;
import Helper.FileCreator;
import Helper.PathHandler;
import driver.FileSystem;


/**
 * 
 * @author Anees Bajwa
 *
 */

public class TestMV {
  Commands mkdir;
  Commands ls;
  MV move;
  CD cd;
  Commands pwd;
  FileSystem system;
  FileCreator creator;
  Commands test;
  PathHandler path;
  FileSystem FS;
  String [] args;
  String executer;

  @Before
  public void setUp() throws Exception {
    mkdir = new MkDir("");
    path = new PathHandler();
    FS = new FileSystem("root");
    ls = new LS("");
    pwd = new PWD("");
    system = new FileSystem("");
    creator = new FileCreator("");
    test = new Commands();
  }

  @Test
  public void testMoveDir() throws Exception{ //test for moving a dir
    args = new String[3];
    args[0] = "a";
    args[1] = "b";
    args[2] = "c";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    move = new MV("mv b a");
    move.execute(FS);
    cd = new CD("cd a");
    FS = cd.changPath(FS);
    executer = ls.execute(FS);
    assertEquals("b", executer);
  }
    
  @Test
  public void testMoveFile() throws Exception{ //test for moving a file
    args = new String[2];
    args[0] = "Folder";
    args[1] = "Folder2";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    
    FS.addFileSystem("a");
    FS.addFileSystem("b");
    Commands main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(FS);
    creator.execute(FS, test.getArguments(), executer);
    ls.setArgument(new String[0]);
    
    move = new MV("mv file Folder");
    move.execute(FS);
    cd = new CD("cd Folder");
    FS = cd.changPath(FS);
    String cuter = ls.execute(FS);
    assertEquals("file", cuter);
  }
  
  @Test
  public void testMoveTwoDir() throws Exception{ //test for moving 2 dires
    args = new String[4];
    args[0] = "Folder";
    args[1] = "Folder2";
    args[2] = "Folder3";
    args[3] = "Folder4";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    move = new MV("mv Folder2 Folder");
    MV move2 = new MV("mv Folder4 Folder");
    move.execute(FS);
    move2.execute(FS);
    cd = new CD("cd Folder");
    FS = cd.changPath(FS);
    executer = ls.execute(FS);
    assertEquals("Folder2\tFolder4", executer);
  }
    
    

}
