Class name: ls.java
Parent class: Command
Responsibility:
	- Display the content of the file in the form of a String
Collaboration:
	- FileSystem
	- path handler