-- ANEES BAJWA: 1001849556
-- JOSEPH CHARLES: 1001535797

DECLARE
c_name DEPT_STAT.dname%type;
TOTAL_EMP DEPT_STAT.TOTAL_EMP%type;
TOTAL_SAL DEPT_STAT.TOTAL_SAL%type;
AVG_SAL DEPT_STAT.AVG_SAL%type;
NUMB number;
CURSOR EMP_INFO is
  SELECT dname,COUNT(DNO) as TOTAL_EMP, SUM(salary) as TOTAL_SAL, ROUND(AVG(salary),2) as AVG_SAL
  FROM EMP NATURAL JOIN DEPT
  GROUP BY dname;

BEGIN
  OPEN EMP_INFO;
  NUMB := 0;
  LOOP
    FETCH EMP_INFO into c_name, TOTAL_EMP, TOTAL_SAL, AVG_SAL;
    EXIT WHEN EMP_INFO%notfound;
    NUMB := NUMB + 1;

    IF TOTAL_EMP <= 3 THEN

      INSERT INTO DEPT_STAT VALUES(c_name, TOTAL_EMP, NULL, NULL, NULL, 'Too few employees');
      dbms_output.put_line('Data ' || NUMB || ' - ' || 'Cannot insert - too few employeees');

    ELSE

      INSERT INTO DEPT_STAT VALUES(c_name, TOTAL_EMP, TOTAL_SAL, AVG_SAL, SEQ_ASSN2_2.nextval, NULL);
      dbms_output.put_line('Data ' || NUMB || ' - ' || 'Successful insertion');

    END IF;
  END LOOP;
  CLOSE EMP_INFO;
END;
/
