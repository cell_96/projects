// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import Helper.*;
import driver.*;

/**
 * 
 * @author Spasimir Vasilev
 *
 */
public class LS extends Commands {
  private PWD pwd = new PWD("");

  public LS(String userInput) {
    super.setInput(userInput); // super is the Command class
  }

  /**
   * List the content according to the user input, if they have -R or not
   * 
   * @param fs is a filesystem object
   * @throws Exception
   * @return empty string, cause of polymorphism
   */
  public String execute(FileSystem fs) throws Exception {
    if (getArguments().length == 0) {
      return fs.getContent().trim(); // if the length is zero, get the content
    } else if (getArgumentAtPosition(0).equals("-R")
        && getArguments().length == 1) {
      return recursiveList(fs, ".").trim(); // If it is a dash R, go through all
    } else if (getArgumentAtPosition(0).equals("-R")) {
      String result = "";
      for (String path : Arrays.copyOfRange(getArguments(), 1,
          getArguments().length)) // Go through
        try {
          result += recursiveList(fs, path);
        } catch (Exception e) { // if the path does not exist
          System.out.println(path + " does not exist");
        }
      return result.trim();
    } else {
      String result = "";
      for (String path : Arrays.copyOfRange(getArguments(), 0,
          getArguments().length)) // Go through the array
        try {
          result += nonRecursiveList(fs, path); // add
        } catch (Exception e) {
          throw new Exception(e.getMessage());
        }
      return result.trim();
    }
  }

  /**
   * Recursive all the directories and return the string off the content
   * 
   * @param fs is a filesystem object
   * @path path is the user input path
   * @throws Exception
   * @return string of all the content (directories)
   */
  private String recursiveList(FileSystem fs, String path) throws Exception {
    String resultant = "";
    fs = getPathHandler().toThePath(fs, path.split("/"), path.startsWith("/"));
    if (fs.getAllChildrenName() == null) { // If nothing
      return "";
    } else if (fs.getChildrenFS() == null) {
      return nonRecursiveList(fs, ".");
    } else {
      resultant += nonRecursiveList(fs, ".");
      for (String dirName : fs.getChildrenFS().keySet()) { // loop through the
                                                           // key
        try { // try block
          resultant += recursiveList(fs, dirName);
        } catch (Exception e) {
          System.out.println(e.getMessage());
        }
      }
      return resultant;
    }
  }

  /**
   * Display the content of the file in the form of a String
   *
   * @param fs file system object
   * @param pathName name of the path
   * @throws Exception
   * @return String of the contents
   */
  private String nonRecursiveList(FileSystem fs, String pathName)
      throws Exception {
    String dirContent = "";
    String[] path;
    path = createNewPath(pathName.split("/"));
    if (path.length > 0) {
      dirContent += listContent(fs, getPathHandler(), path, pathName);
    } else {
      dirContent += "\nDirectory \"" + "/" + "\" has\n"
          + getPathHandler()
              .toThePath(fs, new String[] {}, pathName.startsWith("/"))
              .getContent();
    }
    return dirContent;
  }


  /**
   * List the content of the directories (i.e the within directories)
   *
   * @param fs file system object
   * @param pathHandler pathHandler object
   * @param path is the list of path
   * @param argument is the argument of the command
   * @return a string of the contents
   * @throws Exception
   */
  private String listContent(FileSystem fs, PathHandler pathHandler,
      String[] path, String argument) throws Exception {
    String dirName = path[path.length - 1];
    fs = pathHandler.toThePath(fs, Arrays.copyOfRange(path, 0, path.length - 1),
        argument.startsWith("/"));
    if (fs == null || !fs.getAllChildrenName().contains(dirName)
        && (!dirName.equals(".") && !dirName.equals(".."))) {
      // The other directories, if more than one arguments
      throw new Exception(
          "\nThe Dir \"" + path[path.length - 1] + "\": does not exist");
    } else if (dirName.equals(".")) {
      return "\nDirectory \"" + pwd.execute(fs) + "\" has\n" + fs.getContent();
    } else if (dirName.equals("..") && fs.getParent() != null) {
      fs = fs.getParent();
      return "\nDirectory \"" + pwd.execute(fs) + "\" has\n" + fs.getContent();
    } else if (fs.getChildrenFile().containsKey(dirName)) {
      return dirName;
    } else if (fs.getChildrenFS().containsKey(dirName)) {
      fs = fs.getChildrenFS().get(dirName);
      return "\nDirectory \"" + pwd.execute(fs) + "\" has\n" + fs.getContent();
    }
    return "";
  }

  /**
   * Create s new directory path
   *
   * @param path user path input
   * @return a list of string
   */
  private String[] createNewPath(String[] path) {
    List<String> listPaths = new ArrayList<String>();
    for (String pathName : path) { // Go through the path
      if (!pathName.equals(""))
        // add the path names into an array
        listPaths.add(pathName);
    }
    String[] newPaths = new String[listPaths.size()];
    newPaths = listPaths.toArray(newPaths);
    return newPaths;
  }
}
