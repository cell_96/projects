Class name: Manual
Parent class: None
Subclass: None

Responsibilities:
- Print the manual for the argument that is passed through the man command

Collaborations:
- None