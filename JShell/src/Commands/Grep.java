// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package Commands;

import Commands.*;
import driver.*;
import Helper.*;
import java.util.*;
import java.util.regex.*;

/**
 * 
 * @author Lam
 *
 */
public class Grep extends Commands {
  private boolean recursiveCall;
  private Pattern p;
  private Matcher m;
  private PWD pwd = new PWD("");

  public Grep(String userInput) {
    super.setInput(userInput);
  }

  /**
   * Set the user input to use with this class
   * 
   * @param userInput - a string input
   */
  public void setInput(String userInput) {
    super.setInput(userInput);
    if (getArgumentAtPosition(0).equals("-R")) {
      recursiveCall = true;
      setArgument(Arrays.copyOfRange(getArguments(), 1, getArguments().length));
    } else {
      recursiveCall = false;
    }

  }

  /**
   * Execute the class, checks if its -R or not
   * 
   * @param fs is a filesystem object
   * @throws Exception
   * @return empty string, cause of polymorphism
   */
  public String execute(FileSystem fs) throws Exception {
    if (recursiveCall) {
      return recursiveLineGrabber(fs).trim();
    } else {
      return loopLineGrabber(fs).trim();
    }
  }

  /**
   * Execute the class, checks if its -R or not
   * 
   * @param fs is a filesystem object
   * @param regex is the regex too look for
   * @throws Exception
   * @return string of the lines
   */
  private String lineGrabber(FileSystem fs, String regex) {
    String result = "";
    for (String fileName : fs.getChildrenFile().keySet()) {
      File file = fs.getChildrenFile().get(fileName);
      result += lineGrabber(file, pwd.execute(fs), regex);
    }
    return result;

  }

  /**
   * This is called if -R is not given and its only through a file
   * 
   * @param fs is a filesystem object
   * @throws Exception
   * @return string of all the lines with the word
   */
  private String loopLineGrabber(FileSystem fs) throws Exception {
    String resultant = "";
    String[] convertedPath;
    String[] pathArray;
    String fileName;
    File file;
    for (String path : Arrays.copyOfRange(getArguments(), 1,
        getArguments().length)) {
      try {
        convertedPath = convertPathToArrays(path);
        pathArray =
            Arrays.copyOfRange(convertedPath, 0, convertedPath.length - 1);
        fileName = convertedPath[convertedPath.length - 1];
        FileSystem fsClone =
            getPathHandler().toThePath(fs, pathArray, path.startsWith("/"));
        if (fsClone.isFileExist(fileName)) {
          file = fsClone.getChildrenFile().get(fileName);
        } else {
          throw new Exception(fileName + " is invalid");
        }
        resultant +=
            lineGrabber(file, pwd.execute(fsClone), getArgumentAtPosition(0));
      } catch (Exception e) {
        throw new Exception(e.getMessage());
      }
    }
    return resultant;
  }

  /**
   * Grabs the line from the file
   * 
   * @param file is a File object
   * @param path is the location
   * @param regex is the regex you're looking for in the file
   * @return String of all the lines
   */
  private String lineGrabber(File file, String path, String regex) {
    p = Pattern.compile(regex);
    String resultant = "";
    for (String line : file.getContent().split("\n")) {
      m = p.matcher(line + "\n");
      if (m.find()) {
        if (!path.equals("/"))
          resultant += path + "/" + file.getName() + ": " + line + "\n";
        else
          resultant += "/" + file.getName() + ": " + line + "\n";
      }
    }
    return resultant;
  }

  /**
   * Recursion through all the files, used if -R is called
   * 
   * @param fs is a filesystem object
   * @throws Exception
   * @return string of all the lines
   */
  private String recursiveLineGrabber(FileSystem fs) throws Exception {
    String resultant = "";
    FileSystem fsClone;
    for (String path : Arrays.copyOfRange(getArguments(), 1,
        getArguments().length)) {
      try {
        String[] convertedPath = convertPathToArrays(path);
        if (convertedPath.length >= 1) {
          String fileName = convertedPath[convertedPath.length - 1];
          fsClone = getPathHandler().toThePath(fs,
              Arrays.copyOfRange(convertedPath, 0, convertedPath.length - 1),
              path.startsWith("/"));
          if (fsClone.isFSExist(fileName)) {
            resultant += recusiveCallGrep(fsClone, path);
          } else if (fsClone.isFileExist(fileName)) {
            resultant += lineGrabber(fsClone.getChildrenFile().get(fileName),
                path, getArgumentAtPosition(0));
          } else if (fileName.equals(".") || fileName.equals("..")) {
            fsClone = getPathHandler().toThePath(fsClone,
                new String[] {fileName}, false);
            resultant += recusiveCallGrep(fsClone, path);
          } else
            throw new Exception();
        } else {
          fsClone = getPathHandler().toThePath(fs, new String[] {""}, true);
          resultant += recusiveCallGrep(fsClone, path);
        }
      } catch (Exception e) {
        System.out.println(path + " does not exist");
      }
    }
    return resultant;
  }

  /**
   * -R call, this will recurse through children and all the files
   * 
   * @param fs is a filesystem object
   * @param path is the path the user enters
   * @return string with the contents
   */
  private String recusiveCallGrep(FileSystem fs, String path) {
    if (fs.getChildrenFS() == null) {

      return "";
    } else if (fs.getChildrenFS() == null) {
      return lineGrabber(fs, getArgumentAtPosition(0));
    } else {
      String result = "";
      result += lineGrabber(fs, getArgumentAtPosition(0));
      for (FileSystem fsChild : fsGrabber(fs)) {
        if (fs.getParent() != null)
          result += recusiveCallGrep(fsChild, path + "/" + fsChild.getName());
        else
          result += recusiveCallGrep(fsChild, fsChild.getName());
      }
      return result;
    }
  }

  /**
   * returns an array of the path
   * 
   * @param path is a string
   * @return an array of the path
   */
  private String[] convertPathToArrays(String path) {
    return path.split("/");
  }

  /**
   * Creates an arraylist of filesystem
   * 
   * @param fs is a filesystem object
   * @return arrayList that contains filesystem objects
   */
  private ArrayList<FileSystem> fsGrabber(FileSystem fs) {
    ArrayList<FileSystem> fileSystemArray = new ArrayList<FileSystem>();
    for (String fsName : fs.getChildrenFS().keySet()) {
      fileSystemArray.add(fs.getChildrenFS().get(fsName));
    }

    return fileSystemArray;
  }
}
