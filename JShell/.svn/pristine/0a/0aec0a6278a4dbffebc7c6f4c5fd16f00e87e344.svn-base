package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.Echo;
import Commands.PWD;
import Helper.Commands;
import Helper.Redirector;

public class TestRedirector {
  Redirector red;
  Commands cmd;

  @Before
  public void setUp() throws Exception {
    red = new Redirector();
  }

  @Test
  /*
   * test if getNewCommand returns the proper output with a simple command
   */
  public void testSimpleCommand() {
    cmd = new PWD("");
    String something = "pwd > file";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output with a weird command
   */
  public void TestComplicatedCommand(){
    cmd = new PWD("");
    String something = "pwd >> file > asd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output when there is no 
   * redirection
   */
  public void testCommandNoRedirecton() {
    cmd = new PWD("");
    String something = "pwd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output when the argument list is
   * of length 1
   * 
   */
  public void testCommandWithSmallArgList() {
    cmd = new PWD("");
    String something = "pwd line";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd line", newSomething);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value
   */
  public void TestIsRedirect(){
    cmd = new PWD("");
    String something = "pwd >> file > asd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(true, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when there is no redirection
   */
  public void TestIsNotRedirect(){
    cmd = new PWD("");
    String something = "pwd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when there is no file 
   * specified
   */
  public void TestIsNotRedirectWithNoFile(){
    cmd = new Echo("");
    String something = "echo string >";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when the symbol used is 
   * invalid
   */
  public void TestIsRedirectInvalidSymbol(){
    cmd = new PWD("");
    String something = "pwd >>>> file";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }

  

}
