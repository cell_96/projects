-- ANEES BAJWA: 1001849556
-- JOSEPH CHARLES: 1001535797

CREATE OR REPLACE PROCEDURE generate_id(firstn in VARCHAR, lastn in VARCHAR, bdate IN VARCHAR)
is
age number;
sign VARCHAR2(60);
dob DATE;
BEGIN
  DELETE FROM POSSIBLE_IDS;
  dob := TO_DATE(BDATE,'DD-MON-YYYY');
  age := round(MONTHS_BETWEEN(SYSDATE,dob)/12,0);
  if TO_CHAR(dob,'MM-DD') BETWEEN '03-21' and '04-20' THEN sign := 'Areis';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '04-21' and '05-21' THEN sign := 'Taurus';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '05-22' and '06-21' THEN sign := 'Gemini';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '06-22' and '07-22' THEN sign := 'Cancer';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '07-23' and '08-21' THEN sign := 'Leo';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '08-22' and '09-23' THEN sign := 'Virgo';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '09-24' and '10-23' THEN sign := 'Libra';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '10-24' and '11-22' THEN sign := 'Scorpio';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '11-23' and '12-22' THEN sign := 'Sagittarius';
  elsif TO_CHAR(dob,'mm-DD') BETWEEN '01-21' and '02-19' THEN sign := 'Aquarius';
  elsIF TO_CHAR(dob,'mm-DD') BETWEEN '02-20' and '03-20' THEN  sign := 'Pisces';
  else sign := 'Capricorn';
  END IF;

  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || lastn);
  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || lastn || '_' || age);
  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || age);
  INSERT INTO POSSIBLE_IDS VALUES(lastn || '_' || age);
  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || lastn || '_' || sign);
  INSERT INTO POSSIBLE_IDS VALUES(lastn || '_' || sign);
  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || sign);
  INSERT INTO POSSIBLE_IDS VALUES(firstn || '_' || lastn || '_' || sign || '_' || age);
END;
/
