// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package Commands;

import java.util.*;
import Helper.*;
import driver.*;

public class MV extends Commands {
  private ArrayList<String> specialCharacter =
      new ArrayList<String>(Arrays.asList(".", "..", "/"));

  // Get the user input in the constructor
  public MV(String userInput) {
    super.setInput(userInput);
  }

  /**
   * Move the directory or the file to the new path
   *
   * @param fs filesystem object
   * @return an empty string, used for polymorphism
   * @throws Exception
   */
  public String execute(FileSystem fs) throws Exception {
    String[] oldPath = convertArrayString(getArgumentAtPosition(0));
    String dirNameOldPath = getDirName(getArgumentAtPosition(0));
    String[] newPath = convertArrayString(getArgumentAtPosition(1));
    String dirNameNewPath = getDirName(getArgumentAtPosition(1));
    FileSystem newFS = getPathHandler().toThePath(fs, newPath,
        getArgumentAtPosition(1).startsWith("/"));
    FileSystem oldFS = getPathHandler().toThePath(fs, oldPath,
        getArgumentAtPosition(0).startsWith("/"));
    if ((oldFS.isFSExist(dirNameNewPath) && newFS.isFSExist(dirNameNewPath))
        || specialCharacter.contains(dirNameOldPath)
        || specialCharacter.contains(dirNameNewPath)) {
      moveDirToDir(oldFS, newFS, dirNameOldPath, dirNameNewPath);
    } else if ((oldFS.isFileExist(dirNameOldPath)
        && newFS.isFSExist(dirNameNewPath))
        || specialCharacter.contains(dirNameNewPath)) {
      moveFileToDir(oldFS, newFS, dirNameOldPath, dirNameNewPath);
    } else if (oldFS.isFileExist(dirNameOldPath)
        && newFS.isFileExist(dirNameNewPath)) {
      moveFileToFile(oldFS, newFS, dirNameOldPath, dirNameNewPath);
    } else {
      throw new Exception("MV cannot work"); // throw exception
    }
    return null;
  }

  /**
   * Move file to the new path, and delete from old
   *
   * @param oldFs file system object
   * @param newFs new filesystem object
   * @param dirNameOldPath is the old path
   * @param dirNameNewPath is the new path
   */
  private void moveFileToFile(FileSystem oldFS, FileSystem newFS,
      String dirNameOldPath, String dirNameNewPath) {
    String newContent =
        oldFS.getChildrenFile().get(dirNameOldPath).getContent();
    oldFS.delFile(dirNameOldPath); // delete the file from old one
    newFS.delFile(dirNameNewPath); // delete the file from new one
    newFS.addFile(dirNameOldPath, false, newContent); // add it
  }

  /**
   * Convert the string path to an array
   *
   * @param path user path input
   * @return a list of string
   */
  private String[] convertArrayString(String path) {
    String[] newPath = path.split("/");
    if (newPath.length > 0) {
      int initLen;
      int lastLen = newPath.length - 1;
      if (path.startsWith("/")) {
        initLen = 1;
      } else {
        initLen = 0;
      }
      return Arrays.copyOfRange(newPath, initLen, lastLen);
    } else {
      return new String[] {"."};
    }
  }

  /**
   * get the directory name
   *
   * @param path user path input
   * @return the name of the directory
   */
  private String getDirName(String path) {
    if (path.split("/").length > 0)
      return path.split("/")[path.split("/").length - 1];
    else
      return ".";
  }

  /**
   * Move the dir to dir with deleting the old one
   *
   * @param path is to set path
   * @param oldpath FileSystem object
   * @param newPath FileSystem object
   * @param dirName is the String name of the directory
   * @throws Exception
   */
  private void moveDirToDir(FileSystem oldPath, FileSystem newPath,
      String dirOldName, String dirNewName) throws Exception {
    newPath = getPathHandler().toThePath(newPath, new String[] {dirNewName},
        dirNewName.equals("/"));
    FileSystem old = getPathHandler().toThePath(oldPath,
        new String[] {dirOldName}, dirOldName.equals("/"));
    dirOldName = old.getName();
    newPath.addFileSystem(dirOldName);
    newPath = getPathHandler().toThePath(newPath, new String[] {dirOldName},
        dirOldName.equals("/"));
    newPath.setFSChidlren(old.getChildrenFile(), old.getChildrenFS());
    oldPath.delFs(dirOldName);
  }

  /**
   * Move the file to the directory that the user inputs, and deleter from old
   *
   * @param oldpath FileSystem object
   * @param newPath FileSystem object
   * @param dirOldName is the String name of the old dir
   * @param dirNewPath is the path of the new one
   * @throws Exception
   * 
   */
  private void moveFileToDir(FileSystem oldPath, FileSystem newPath,
      String dirOldName, String dirNewPath) throws Exception {
    newPath = // Set up for the new path
        getPathHandler().toThePath(newPath, new String[] {dirNewPath},
            dirNewPath.equals("/"));
    if (newPath.getAllChildrenName().contains(dirOldName)) {
      return; // throw exception
    } else {
      newPath.addFile(dirOldName, true,
          oldPath.getChildrenFile().get(dirOldName).getContent());
      oldPath.delFile(dirOldName);
    }
  }
}
